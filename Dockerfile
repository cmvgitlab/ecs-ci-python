ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}

RUN curl -o docker-19.03.5.tgz https://download.docker.com/linux/static/stable/x86_64/docker-19.03.5.tgz
RUN tar xf docker-19.03.5.tgz
RUN cp docker/* /usr/bin
RUN chmod a+x /usr/bin/docker*

RUN apt-get update
RUN apt-get install build-essential

RUN pip install --upgrade pip
RUN pip install docker-compose
RUN pip install awscli

COPY scripts scripts
RUN chmod a+x scripts/*