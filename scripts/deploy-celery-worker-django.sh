#!/usr/bin/env bash

# Deploys a docker container into ECS for a celery worker
#
# Environment variables required:
#
# CI_COMMIT_REF_NAME        - Set by Gitlab CI
# SHORT_NAME                - The short name of the project. Unique to project but same across installations.
# IMAGE_NAME                - Name of the image in the AWS container registry.
# SECRET_ARN                - AWS ARN for the Secret containing settings.
# ENTRY_POINT               - Entry Point for the container, defaults to: /home/${ShortName}/app/.venv/bin/celery
# ENVIRONMENT_NAME          - The name of the environment. This can be something like "staging" or "production"
# 

STACK_ACTION=$(aws cloudformation describe-stacks --stack-name $SHORT_NAME-celery-worker &>/dev/null && echo "update-stack" || echo "create-stack")
aws s3 cp s3://cmv-cloudformation/ecs/services/website/scripts/build_parameters.py build_parameters.py
EXTRA_PARAMETERS=$(aws cloudformation describe-stacks --stack-name cmv-ecs-prod | python build_parameters.py ClusterName)

if [[ -n "${ENTRY_POINT}" ]]; then
    EXTRA_PARAMETERS="ParameterKey=EntryPoint,ParameterValue=${ENTRY_POINT} ${EXTRA_PARAMETERS}"
fi

aws cloudformation $STACK_ACTION --stack-name $SHORT_NAME-celery-worker --capabilities "CAPABILITY_IAM" "CAPABILITY_NAMED_IAM" --template-url https://s3-ap-southeast-2.amazonaws.com/cmv-cloudformation/ecs/services/website/celery-service-django.yaml --parameters ParameterKey=ShortName,ParameterValue=$SHORT_NAME ParameterKey=SecretArn,ParameterValue=$SECRET_ARN ParameterKey=ImageName,ParameterValue=$IMAGE_NAME ParameterKey=ContainerTag,ParameterValue=$CI_COMMIT_REF_NAME ParameterKey=EnvironmentName,ParameterValue=$ENVIRONMENT_NAME $EXTRA_PARAMETERS