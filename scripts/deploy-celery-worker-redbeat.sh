#!/usr/bin/env bash

# Deploys a docker container into ECS for a celery worker
#
# Environment variables required:
#
# CI_COMMIT_REF_NAME        - Set by Gitlab CI
# SHORT_NAME                - The short name of the project. Unique to project but same across installations.
# IMAGE_NAME                - Name of the image in the AWS container registry.
# SECRET_ARN                - AWS ARN for the Secret containing settings.
# 

export STACK_ACTION=$(aws cloudformation describe-stacks --stack-name $SHORT_NAME-celery-worker &>/dev/null && echo "update-stack" || echo "create-stack")
aws s3 cp s3://cmv-cloudformation/ecs/services/website/scripts/build_parameters.py build_parameters.py
export EXTRA_PARAMETERS=$(aws cloudformation describe-stacks --stack-name cmv-ecs-prod | python build_parameters.py ClusterName)
aws cloudformation $STACK_ACTION --stack-name $SHORT_NAME-celery-worker --capabilities "CAPABILITY_IAM" "CAPABILITY_NAMED_IAM" --template-url https://s3-ap-southeast-2.amazonaws.com/cmv-cloudformation/ecs/services/website/celery-service-redbeat.yaml --parameters ParameterKey=ShortName,ParameterValue=$SHORT_NAME ParameterKey=SecretArn,ParameterValue=$SECRET_ARN ParameterKey=ImageName,ParameterValue=$IMAGE_NAME ParameterKey=ContainerTag,ParameterValue=$CI_COMMIT_REF_NAME $EXTRA_PARAMETERS