#!/usr/bin/env sh

# Stages a docker container for deployment into ECS for a django-based website
# Django Configuration documented here: https://intranet.cmv.com.au/display/SD/Django+Configuration+for+ECS+Hosting
#
# Environment variables required:
#
# CI_JOB_TOKEN              - Set by Gitlab CI
# CI_COMMIT_REF_NAME        - Set by Gitlab CI
# IMAGE_NAME                - Name of the image in the AWS container registry.
# REGISTRY_URL              - URL for the Gitlab Container Registry. This should be something like: docker-registry.cmv.com.au/<groupname>/<project-name>
# 

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN docker-registry.cmv.com.au

docker pull $REGISTRY_URL:$CI_COMMIT_REF_NAME
docker tag $REGISTRY_URL:$CI_COMMIT_REF_NAME $REGISTRY_URL:$CI_COMMIT_REF_NAME
docker push $REGISTRY_URL:$CI_COMMIT_REF_NAME
docker tag $REGISTRY_URL:$CI_COMMIT_REF_NAME 691908636323.dkr.ecr.ap-southeast-2.amazonaws.com/$IMAGE_NAME:$CI_COMMIT_REF_NAME
$(aws ecr get-login --no-include-email --region ap-southeast-2)
docker push 691908636323.dkr.ecr.ap-southeast-2.amazonaws.com/$IMAGE_NAME:$CI_COMMIT_REF_NAME
