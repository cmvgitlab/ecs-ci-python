#!/usr/bin/env sh

# Builds a docker container for a django-based website
# Django Configuration documented here: https://intranet.cmv.com.au/display/SD/Django+Configuration+for+ECS+Hosting
#
# Environment variables required:
#
# CI_JOB_TOKEN              - Set by Gitlab CI
# CI_COMMIT_REF_NAME        - Set by Gitlab CI
# PROJECT_NAME              - Name of the project. Unique to project but same across installations.
# REGISTRY_URL              - URL for the Gitlab Container Registry. This should be something like: docker-registry.cmv.com.au/<groupname>/<project-name>
# 

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN docker-registry.cmv.com.au
docker build --build-arg PROJECT_NAME=$PROJECT_NAME -t $REGISTRY_URL:$CI_COMMIT_REF_NAME .
docker push $REGISTRY_URL:$CI_COMMIT_REF_NAME
