#!/usr/bin/env bash

# Deploys a docker container into ECS for a django-based website
# Django Configuration documented here: https://intranet.cmv.com.au/display/SD/Django+Configuration+for+ECS+Hosting
#
# Environment variables required:
#
# CI_JOB_TOKEN              - Set by Gitlab CI
# CI_COMMIT_REF_NAME        - Set by Gitlab CI
# ENVIRONMENT_NAME          - Unique name per installation. This can be used to build the name of the settings secret, for example. 
# SHORT_NAME                - The short name of the project. Unique to project but same across installations.
# IMAGE_NAME                - Name of the image in the AWS container registry.
# REGISTRY_URL              - URL for the Gitlab Container Registry. This should be something like: docker-registry.cmv.com.au/<groupname>/<project-name>
# SECRET_ARN                - AWS ARN for the Secret containing settings.
# DOMAIN_NAME               - Domain name at which the site will be accessible.
# LISTENER_ARN              - ARN for the ALB Listener to attach to
# LISTENER_RULE_PRIORITY    - Integer for Load Balancer listener priority. This needs to be unique for all installations per Listener.
# CERTIFICATE_VALIDATION_DOMAIN - Certificate validation emails will go to webmaster@$CERTIFICATE_VALIDATION_DOMAIN
# SUBJECT_ALTERNATIVE_NAMES - Alternative names on the SSL certificate.
# MEDIA_REQUIRED            - true if Media Bucket is required, false if not.
# PRIVATE_STORAGE_REQUIRED  - true if Private Storage is required, false if not.
# DESIRED_CONTAINER_COUNT   - Number of containers to deploy
#

cat /proc/$$/comm

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN docker-registry.cmv.com.au
STACK_ACTION=$(aws cloudformation describe-stacks --stack-name $SHORT_NAME-website &>/dev/null && echo "update-stack" || echo "create-stack")
aws s3 cp s3://cmv-cloudformation/ecs/services/website/scripts/build_parameters.py build_parameters.py

if [[ -z "${LISTENER_ARN}" ]]; then
    EXTRA_PARAMETERS=$(aws cloudformation describe-stacks --stack-name cmv-ecs-prod | python build_parameters.py ClusterName,VpcId,ListenerArn)
else
    EXTRA_PARAMETERS=$(aws cloudformation describe-stacks --stack-name cmv-ecs-prod | python build_parameters.py ClusterName,VpcId)
    EXTRA_PARAMETERS="ParameterKey=ListenerArn,ParameterValue=${LISTENER_ARN} ${EXTRA_PARAMETERS}"
fi

if [[ -z "${MEDIA_REQUIRED}" ]]; then
    EXTRA_PARAMETERS="ParameterKey=MediaRequired,ParameterValue=true ${EXTRA_PARAMETERS}"
else
    EXTRA_PARAMETERS="ParameterKey=MediaRequired,ParameterValue=${MEDIA_REQUIRED} ${EXTRA_PARAMETERS}"
fi

if [[ -z "${PRIVATE_STORAGE_REQUIRED}" ]]; then
    EXTRA_PARAMETERS="ParameterKey=PrivateStorage,ParameterValue=false ${EXTRA_PARAMETERS}"
else
    EXTRA_PARAMETERS="ParameterKey=PrivateStorage,ParameterValue=${PRIVATE_STORAGE_REQUIRED} ${EXTRA_PARAMETERS}"
fi

if [[ -z "${DESIRED_CONTAINER_COUNT}" ]]; then
    EXTRA_PARAMETERS="ParameterKey=DesiredContainerCount,ParameterValue=1 ${EXTRA_PARAMETERS}"
else
    EXTRA_PARAMETERS="ParameterKey=DesiredContainerCount,ParameterValue=${DESIRED_CONTAINER_COUNT} ${EXTRA_PARAMETERS}"
fi

docker run -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY -e ENVIRONMENT_NAME -e SHORT_NAME $REGISTRY_URL:$CI_COMMIT_REF_NAME migrate
aws cloudformation $STACK_ACTION --stack-name $SHORT_NAME-website --capabilities "CAPABILITY_IAM" "CAPABILITY_NAMED_IAM" --template-url https://s3-ap-southeast-2.amazonaws.com/cmv-cloudformation/ecs/services/website/website-service.yaml --parameters ParameterKey=ShortName,ParameterValue=$SHORT_NAME ParameterKey=EnvironmentName,ParameterValue=$ENVIRONMENT_NAME ParameterKey=SecretArn,ParameterValue=$SECRET_ARN ParameterKey=DomainName,ParameterValue=$DOMAIN_NAME ParameterKey=ListenerRulePriority,ParameterValue=$LISTENER_RULE_PRIORITY ParameterKey=CertificateValidationDomain,ParameterValue=$CERTIFICATE_VALIDATION_DOMAIN ParameterKey=SubjectAlternativeNames,ParameterValue=$SUBJECT_ALTERNATIVE_NAMES ParameterKey=ImageName,ParameterValue=$IMAGE_NAME ParameterKey=ContainerTag,ParameterValue=$CI_COMMIT_REF_NAME $EXTRA_PARAMETERS
