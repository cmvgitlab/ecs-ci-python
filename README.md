# ecs-ci-python Docker Image

This project provides a docker image to be used to build containers for use in Amazon ECS for Python projects. The CI for this project builds the image and uploads it to the Gitlab Container Registry.

Credentials will need to be provided as Gitlab Secret Variables (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY) to provide access to the Amazon Elastic Container Registry.

Use this as the base image in another `.gitlab-ci.yml` file, with the image tag being the minor python version, like so:

```yaml
build:
    image: docker-registry.cmv.com.au/containers/ecs-ci-python:3.7
    script:
        - your CI script
        - goes here
```
